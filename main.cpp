#include <iostream>
#include <fstream>
#include <string>
#include <regex>
#include <hpdf.h>

using namespace std;

struct markdown_pattern
{
    string header_pattern;
    string bold_pattern;
    string italic_pattern;
    string syntax1_pattern;
};

struct file_style
{
    string file_name;
};

struct row_style
{
    bool h1_style;
    bool h2_style;
    bool h3_style;
    bool h4_style;
    bool h5_style;
    bool h6_style;
};

void hpdf_error_handler(
        HPDF_STATUS error_no,
        HPDF_STATUS detail_no,
        void* user_data)
{
    char message[1024] = {};
    sprintf(message, "error: error_no = %04X, detail_no = %d",
            static_cast<unsigned int>(error_no), static_cast<int>(detail_no));
    throw runtime_error(message);
}

void initstruct(
        markdown_pattern *pattern)
{
    pattern -> header_pattern = "^[ |\t|]*#{1,6}";
}

void debugstruct(
        markdown_pattern *pattern)
{
    printf("--- BEGIN: struct debug console ---\n");
    printf("header pattern: %s\n", pattern -> header_pattern.c_str());
    printf("bold pattern: %s\n", pattern -> bold_pattern.c_str());
    printf("italic pattern: %s\n", pattern -> italic_pattern.c_str());
    printf("syntax1 pattern: %s\n", pattern -> syntax1_pattern.c_str());
    printf("--- END: struct debug console ---\n");
}

int mkdanalysis(
        string line,
        file_style *fstyle,
        row_style *rstyle,
        markdown_pattern *pattern)
{
    regex reg(pattern -> header_pattern);
    if (regex_search(line.begin(), line.end(), reg)) {
        cout << "match!!" << endl;
    }

    return 0;
}

int mkd2bitbucket(
        string fname,
        markdown_pattern *pattern)
{
    ifstream ifs(fname.c_str());
    if (ifs.fail()) { return -1; }

    //initialize file style
    file_style fstyle;

    string line;
    int linecnt = 1;
    while (getline(ifs, line)) {

        //initialize row style
        row_style rstyle;

        printf("--- LINE NO: %d\n", linecnt);
        printf("INPUT TEXT: %s\n", line.c_str());

        //analysis markdown style
        mkdanalysis(line, &fstyle, &rstyle, pattern);

        printf("\n");
        linecnt++;
    }
}

int main(
        int arg,
        char *args[])
{
    markdown_pattern pattern;
    initstruct(&pattern);
    debugstruct(&pattern);

    mkd2bitbucket("header.md", &pattern);
}

